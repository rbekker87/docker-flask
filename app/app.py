import socket
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hostname: {}, deployed with Docker and Flask'.format(socket.gethostname())

if __name__ == '__main__': 
    app.run(host='0.0.0.0')
